package getgps.jspblm.com.getgps.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import getgps.jspblm.com.getgps.R;

/**
 * Created by jspbl on 22/11/2016.
 */
public class Derecha extends Fragment {
    View rootView;
    TextView txt;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.derecha,container,false);
        txt = (TextView) rootView.findViewById(R.id.txt);

        return rootView;
    }

    public void obtenerDatos(String mensaje) {
        txt.setText(mensaje);
    }

}
