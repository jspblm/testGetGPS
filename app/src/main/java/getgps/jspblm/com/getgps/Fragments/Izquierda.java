package getgps.jspblm.com.getgps.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import getgps.jspblm.com.getgps.EnviarMensaje;
import getgps.jspblm.com.getgps.R;
import getgps.jspblm.com.getgps.Tools.LocationService;

/**
 * Created by jspbl on 22/11/2016.
 */
public class Izquierda extends Fragment {
    View rootView;
    EditText campo;
    Button boton;
    Button gpsButton;
    TextView gpsData;
    EnviarMensaje EM;
//    private ProgressBar spinner;
    ProgressDialog progressDialog;
    Location cords = null;
    Handler handler = new Handler();
    LocationService locationService;
    int countTryGetGPS = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.izquierda, container, false);

        campo = (EditText) rootView.findViewById(R.id.campotxt);
        boton = (Button) rootView.findViewById(R.id.boton);
        gpsButton = (Button) rootView.findViewById(R.id.gps_button);
        gpsData = (TextView) rootView.findViewById(R.id.gps_data);

        locationService = new LocationService(getActivity());


        boton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle("Wait");
                progressDialog.setMessage("Please Wait");
                progressDialog.setCancelable(false);
                progressDialog.show();
                checkGPS();
            }
        });

        gpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.cancel();
            }
        });

        return rootView;
    }

    public void checkGPS() {
        countTryGetGPS++;
        Log.i("getGPS", "checkGPS " + countTryGetGPS);
        if (countTryGetGPS < 3) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    cords = locationService.getCords();
                    if (cords != null) {
                        Log.i("getGPS", cords.getLatitude() + "," + cords.getLongitude());
                        Log.i("getGPS", "(i) progressDialog Cancel");
                        progressDialog.cancel();
                        EM.enviarDatos(cords.getLatitude() + "," + cords.getLongitude());
                        countTryGetGPS = 0;
                    } else {
                        checkGPS();
                    }
                }
            }, 2 * 1000);
        } else {
            progressDialog.cancel();
            countTryGetGPS = 0;
            Toast toast = Toast.makeText(getActivity(), "No se puede obtener GPS", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            EM = (EnviarMensaje) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Necesitas implementar");
        }


    }

}
