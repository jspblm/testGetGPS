package getgps.jspblm.com.getgps.Tools;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by jspbl on 22/11/2016.
 */
public class LocationService extends Service implements LocationListener {

    private final Context mContext;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location;
    double latitude = 0.0;
    double longitude = 0.0;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    protected LocationManager locationManager;

    public LocationService(Context context) {
        this.mContext = context;
        getLocation();
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                CharSequence netWorkError = "No Network";
                Toast toast = Toast.makeText(mContext, netWorkError, Toast.LENGTH_SHORT);
                toast.show();
                Log.d("getGPS", "no network provider is enabled");
            } else {
                this.canGetLocation = true;

                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("getGPS", "GPS Enabled");
                    }
                }

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("getGPS", "Network Enabled");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Location getCords() {
        Log.d("getGPS", latitude + "," + longitude);
        if (latitude == 0.0 && longitude == 0.0) {
            Log.d("getGPS", "getCoords empty");
            return null;
        }
        return location;
//        return latitude + "," + longitude;
    }

    public void stopUsingGPS() {
        Log.d("getGPS", "stopUsingGPS");
        if (locationManager != null) {
            Log.d("getGPS", "removeUpdates");
            locationManager.removeUpdates(LocationService.this);
        }
    }

    public double getLatitude() {
        latitude = location.getLatitude();
        Log.i("getGPS", "-- getLatitude " + latitude);
        return latitude;
    }

    public double getLongitude() {
        longitude = location.getLongitude();
        Log.i("getGPS", "-- getLongitude " + longitude);
        return longitude;
    }

    public boolean catGetLocation() {
        return this.canGetLocation;
    }

    @Override
    public void onLocationChanged(Location actualLocation ) {
        location = actualLocation ;
        longitude = (location.getLongitude());
        latitude = (location.getLatitude());

        Log.i("getGPS", "**** (d) onLocationChanged lat: " + latitude + " lon: " + longitude);
        Log.i("getGPS", latitude + "," + longitude);
        this.stopUsingGPS();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("getGPS", "[Error] onProviderDisabled");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mContext.startActivity(intent);
    }

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}
